# NBA Players App

This is an Android native application that is connected to [BallDontLie](https://www.balldontlie.io/) 
remote API. The app features list of NBA players with pagination, from there users can go 
to player detail screen and from there to player's team detail screen by clicking team's name.

Architectures and libraries used in this app:
MVI, repository pattern, Koin, Moshi, Retrofit, Room, Compose UI, Paging3, Glide

To make network requests work sign in and get your API key here https://new.balldontlie.io/ 
then open local.properties file and put this line there:

BALL_DONT_LIE_API_KEY=YOUR_API_KEY

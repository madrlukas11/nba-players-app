package com.l00k.nbaplayersapp.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [
        PlayerEntity::class,
        TeamEntity::class,
    ],
    version = 1
)
abstract class NbaPlayersDatabase : RoomDatabase() {

    abstract val playersDao: PlayersDao
    abstract val teamsDao: TeamsDao
}
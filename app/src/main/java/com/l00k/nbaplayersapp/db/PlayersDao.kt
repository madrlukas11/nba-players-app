package com.l00k.nbaplayersapp.db

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert

@Dao
interface PlayersDao {

    @Upsert
    suspend fun upsertAll(players: List<PlayerEntity>)

    @Query("SELECT * FROM players WHERE id = :id")
    fun getPlayerById(id: Long): PlayerEntity?

    @Query("DELETE FROM players")
    fun clearAll()
}
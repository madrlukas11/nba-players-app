package com.l00k.nbaplayersapp.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "teams")
data class TeamEntity(
    @PrimaryKey val id: Long,
    val name: String,
    val city: String,
    val conference: String,
    val division: String,
    val abbreviation: String,
)

package com.l00k.nbaplayersapp.db

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert

@Dao
interface TeamsDao {

    @Upsert
    suspend fun upsertAll(teams: List<TeamEntity>)

    @Query("SELECT * FROM teams WHERE id = :id")
    fun getTeamById(id: Long): TeamEntity

    @Query("DELETE FROM teams")
    fun clearAll()
}
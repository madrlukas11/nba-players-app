package com.l00k.nbaplayersapp.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "players")
data class PlayerEntity(
    @PrimaryKey val id: Long,
    val firstName: String,
    val lastName: String,
    val position: String,
    val height: String,
    val weight: String,
    val jerseyNumber: String,
    val college: String,
    val country: String,
    val draftYear: Int?,
    val draftRound: Int?,
    val draftNumber: Int?,
    val team: String,
    val teamId: Long
)
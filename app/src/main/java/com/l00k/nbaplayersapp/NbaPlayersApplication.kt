package com.l00k.nbaplayersapp

import android.app.Application
import com.l00k.nbaplayersapp.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class NbaPlayersApplication : Application() {

    override fun onCreate() {
        startKoin {
            androidContext(this@NbaPlayersApplication)
            modules(appModule)
        }
        super.onCreate()
    }
}
package com.l00k.nbaplayersapp.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface BallDontLieApi {

    @GET("/v1/players")
    suspend fun getPlayers(
        @Query("cursor") nextCursor: Int,
        @Query("per_page") itemsPerPage: Int = 35,
    ): Response<PlayersData>
}
package com.l00k.nbaplayersapp.api

import com.squareup.moshi.Json

/**
 * Network data transfer object for one page of players.
 *
 * @property players list of players
 * @property pagingData data used for paging network requests
 */
data class PlayersData(
    @field:Json(name = "data") val players: List<Player>,
    @field:Json(name = "meta") val pagingData: PagingMetadata,
)

/**
 * Player data object.
 */
data class Player(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "first_name") val firstName: String,
    @field:Json(name = "last_name") val lastName: String,
    @field:Json(name = "position") val position: String,
    @field:Json(name = "height") val height: String,
    @field:Json(name = "weight") val weight: String,
    @field:Json(name = "jersey_number") val jerseyNumber: String,
    @field:Json(name = "college") val college: String,
    @field:Json(name = "country") val country: String,
    @field:Json(name = "draft_year") val draftYear: Int?,
    @field:Json(name = "draft_round") val draftRound: Int?,
    @field:Json(name = "draft_number") val draftNumber: Int?,
    @field:Json(name = "team") val team: Team,
)

/**
 * Team data object.
 */
data class Team(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "full_name") val name: String,
    @field:Json(name = "city") val city: String,
    @field:Json(name = "conference") val conference: String,
    @field:Json(name = "division") val division: String,
    @field:Json(name = "abbreviation") val abbreviation: String,
)

/**
 * Data used for player paging network requests.
 *
 * @property nextCursor current last item from which next page will be counted
 * @property perPage number of items to return in a response
 */
data class PagingMetadata(
    @field:Json(name = "next_cursor") val nextCursor: Int,
    @field:Json(name = "per_page")val perPage: Int,
)

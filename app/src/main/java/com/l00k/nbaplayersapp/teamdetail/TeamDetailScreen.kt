package com.l00k.nbaplayersapp.teamdetail

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.l00k.nbaplayersapp.R
import com.l00k.nbaplayersapp.ui.MockData
import com.l00k.nbaplayersapp.ui.base.BaseViewModel
import com.l00k.nbaplayersapp.ui.base.PreviewSurface
import com.l00k.nbaplayersapp.ui.base.PreviewViewModel
import com.l00k.nbaplayersapp.ui.base.UIEvent
import com.l00k.nbaplayersapp.ui.base.UIState
import com.l00k.nbaplayersapp.ui.component.InfoItem
import com.l00k.nbaplayersapp.ui.component.NbaPlayersAppImage
import com.l00k.nbaplayersapp.ui.component.ScreenStateWrapper
import com.l00k.nbaplayersapp.ui.component.Toolbar

@Composable
fun TeamDetailScreen(
    navController: NavController,
    viewModel: BaseViewModel<TeamDetailScreenData, UIEvent> // TeamDetailViewModel
) {
    val viewState = viewModel.viewState.collectAsStateWithLifecycle()

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Toolbar(
            title = stringResource(id = R.string.team_detail),
            navController = navController
        )
        viewState.value.data.team?.let { team ->
            ScreenStateWrapper(state = viewState.value) {
                Box(
                    modifier = Modifier
                        .clip(RoundedCornerShape(12.dp))
                        .background(color = MaterialTheme.colorScheme.primaryContainer)
                        .padding(20.dp)
                ) {
                    Column(
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        NbaPlayersAppImage(
                            modifier = Modifier
                                .size(100.dp)
                                .align(Alignment.CenterHorizontally),
                            imageUrl = null,
                            contentDescription = stringResource(id = R.string.team_image)
                        )
                        Text(
                            modifier = Modifier
                                .align(Alignment.CenterHorizontally)
                                .padding(top = 20.dp),
                            text = team.name,
                            color = MaterialTheme.colorScheme.primary,
                            fontSize = 22.sp,
                            fontWeight = FontWeight.W500
                        )
                        HorizontalDivider(
                            modifier = Modifier.padding(vertical = 16.dp),
                            color = MaterialTheme.colorScheme.secondary
                        )
                        InfoItem(
                            modifier = Modifier.padding(top = 12.dp),
                            title = stringResource(id = R.string.city),
                            value = team.city
                        )
                        InfoItem(
                            modifier = Modifier.padding(top = 12.dp),
                            title = stringResource(id = R.string.conference),
                            value = team.conference
                        )
                        InfoItem(
                            modifier = Modifier.padding(top = 12.dp),
                            title = stringResource(id = R.string.division),
                            value = team.division
                        )
                        InfoItem(
                            modifier = Modifier.padding(top = 12.dp),
                            title = stringResource(id = R.string.abbreviation),
                            value = team.abbreviation
                        )
                    }
                }
            }
        }
    }
}

@PreviewLightDark
@Composable
private fun TeamDetailScreenPreview() {
    PreviewSurface {
        TeamDetailScreen(
            navController = rememberNavController(),
            viewModel = PreviewViewModel(
                UIState(
                    data = TeamDetailScreenData(
                        team = MockData.teamDetail
                    )
                )
            )
        )
    }
}

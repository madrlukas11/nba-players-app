package com.l00k.nbaplayersapp.teamdetail

import com.l00k.nbaplayersapp.ui.model.TeamDetailVo

/**
 * Data holder class to be displayed on TeamDetailScreen.
 *
 * @property team team detail data to be displayed to user
 */
data class TeamDetailScreenData(
    val team: TeamDetailVo?,
)
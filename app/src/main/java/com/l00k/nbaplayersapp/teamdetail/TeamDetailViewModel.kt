package com.l00k.nbaplayersapp.teamdetail

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.l00k.nbaplayersapp.repository.TeamsRepository
import com.l00k.nbaplayersapp.toTeamDetailVo
import com.l00k.nbaplayersapp.ui.base.BaseViewModel
import com.l00k.nbaplayersapp.ui.base.UIEvent
import com.l00k.nbaplayersapp.ui.base.UIState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class TeamDetailViewModel(
    teamId: Long,
    private val teamsRepository: TeamsRepository,
) : BaseViewModel<TeamDetailScreenData, UIEvent>, ViewModel() {

    init {
        viewModelScope.launch {
            teamsRepository.getTeamById(teamId)?.toTeamDetailVo()?.let { team ->
                _viewState.update {
                    UIState(
                        data = TeamDetailScreenData(team = team),
                        isLoading = false
                    )
                }
            }
        }
    }

    private val _viewState = MutableStateFlow(
        UIState(
            data = TeamDetailScreenData(team = null),
            isLoading = true
        )
    )
    override val viewState = _viewState

    override fun onEvent(event: UIEvent) {
        Log.d(
            "TeamDetailViewModel",
            "${this::class.java.simpleName} does not accept any UI events"
        )
    }
}
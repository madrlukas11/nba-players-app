package com.l00k.nbaplayersapp.di

import android.content.Context
import androidx.room.Room.databaseBuilder
import com.l00k.nbaplayersapp.BuildConfig
import com.l00k.nbaplayersapp.api.BallDontLieApi
import com.l00k.nbaplayersapp.db.NbaPlayersDatabase
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val BASE_API_URL = "https://api.balldontlie.io"

fun provideBallDontLieApi(): BallDontLieApi {
    return Retrofit.Builder().apply {
        baseUrl(BASE_API_URL)
        addConverterFactory(MoshiConverterFactory.create())
        client(getOkHttpClient())
    }.build().create(BallDontLieApi::class.java)
}

fun provideDatabase(context: Context) =
    databaseBuilder(
        context,
        NbaPlayersDatabase::class.java,
        "NbaPlayersDb"
    ).fallbackToDestructiveMigration()
        .build()


private fun getOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder().apply {
        addInterceptor(
            HttpLoggingInterceptor().apply {
                setLevel(HttpLoggingInterceptor.Level.BODY)
            }
        )
        addInterceptor(Interceptor {
            val originalRequest = it.request()
            val requestBuilder = originalRequest.newBuilder()
                .header("Authorization", BuildConfig.BALL_DONT_LIE_API_KEY)
            val request = requestBuilder.build()
            it.proceed(request)
        })
    }.build()
}
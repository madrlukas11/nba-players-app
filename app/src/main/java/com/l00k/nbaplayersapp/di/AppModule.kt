package com.l00k.nbaplayersapp.di

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingSource
import com.l00k.nbaplayersapp.api.BallDontLieApi
import com.l00k.nbaplayersapp.api.Player
import com.l00k.nbaplayersapp.playerdetail.PlayerDetailViewModel
import com.l00k.nbaplayersapp.playerlist.PlayerListViewModel
import com.l00k.nbaplayersapp.playerlist.PlayersPagingSource
import com.l00k.nbaplayersapp.repository.PlayersRepository
import com.l00k.nbaplayersapp.repository.PlayersRepositoryImpl
import com.l00k.nbaplayersapp.repository.TeamsRepository
import com.l00k.nbaplayersapp.repository.TeamsRepositoryImpl
import com.l00k.nbaplayersapp.teamdetail.TeamDetailViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val appModule = module {

    single<BallDontLieApi> {
        provideBallDontLieApi()
    }

    single {
        provideDatabase(context = androidContext())
    }

    single<PlayersRepository> {
        PlayersRepositoryImpl(
            remoteApi = get(),
            database = get()
        )
    }

    single<TeamsRepository> {
        TeamsRepositoryImpl(database = get())
    }

    single<PagingSource<Int, Player>> {
        PlayersPagingSource(
            playersRepository = get(),
            teamsRepository = get()
        )
    }

    single<Pager<Int, Player>> {
        Pager(
            config = PagingConfig(pageSize = 35),
            pagingSourceFactory = { get() }
        )
    }

    viewModelOf(::PlayerListViewModel)
    viewModelOf(::PlayerDetailViewModel)
    viewModelOf(::TeamDetailViewModel)
}
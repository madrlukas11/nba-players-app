package com.l00k.nbaplayersapp.repository

import com.l00k.nbaplayersapp.api.Player
import com.l00k.nbaplayersapp.api.PlayersData
import com.l00k.nbaplayersapp.db.PlayerEntity

/**
 * Repository executing operations concerning [Player]
 */
interface PlayersRepository {

    /**
     * Fetches players data from remote API
     *
     * @param nextCursor last item position which the next page is counted from
     */
    suspend fun fetchPlayers(
        nextCursor: Int,
    ): PlayersData?

    /**
     * Gets player by his id from cache
     */
    suspend fun getPlayerById(id: Long): PlayerEntity?

    /**
     * Saves players to cache
     *
     * @param players list of players to save
     * @param clearFirst whether player cache should be cleared before saving new players
     */
    suspend fun savePlayers(
        players: List<Player>,
        clearFirst: Boolean = false,
    )

    /**
     * Removes all players from cache
     */
    suspend fun clearPlayers()
}
package com.l00k.nbaplayersapp.repository

import android.util.Log
import androidx.room.withTransaction
import com.l00k.nbaplayersapp.api.BallDontLieApi
import com.l00k.nbaplayersapp.api.Player
import com.l00k.nbaplayersapp.api.PlayersData
import com.l00k.nbaplayersapp.db.NbaPlayersDatabase
import com.l00k.nbaplayersapp.db.PlayerEntity
import com.l00k.nbaplayersapp.toPlayerEntity

class PlayersRepositoryImpl(
    private val remoteApi: BallDontLieApi,
    private val database: NbaPlayersDatabase,
) : PlayersRepository {

    override suspend fun fetchPlayers(
        nextCursor: Int,
    ): PlayersData? = remoteApi.getPlayers(
        nextCursor = nextCursor
    ).body()

    override suspend fun getPlayerById(id: Long): PlayerEntity? {
        return try {
            database.withTransaction {
                database.playersDao.getPlayerById(id)
            }
        } catch (ex: Exception) {
            Log.e("PlayersRepositoryImpl","Could not get player by id $id: ${ex.message}")
            null
        }
    }

    override suspend fun savePlayers(
        players: List<Player>,
        clearFirst: Boolean
    ) {
        try {
            database.withTransaction {
                database.playersDao.upsertAll(players.toPlayerEntity())
            }
        } catch (ex: Exception) {
            Log.w("PlayersRepositoryImpl","Could not save players: ${ex.message}")
        }
    }

    override suspend fun clearPlayers() {
        try {
            database.withTransaction {
                database.playersDao.clearAll()
            }
        } catch (ex: Exception) {
            Log.w("PlayersRepositoryImpl", "Could not clear players: ${ex.message}")
        }
    }
}
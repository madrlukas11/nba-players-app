package com.l00k.nbaplayersapp.repository

import com.l00k.nbaplayersapp.api.Team
import com.l00k.nbaplayersapp.db.TeamEntity

/**
 * Repository executing operations concerning [Team]
 */
interface TeamsRepository {

    /**
     * Gets team by its id from cache
     *
     * @param id unique identifier to find the team
     */
    suspend fun getTeamById(id: Long): TeamEntity?

    /**
     * Saves teams to cache
     *
     * @param teams list of teams to save
     * @param clearFirst whether team cache should be cleared before saving new players
     */
    suspend fun saveTeams(
        teams: List<Team>,
        clearFirst: Boolean = false,
    )

    /**
     * Removes all teams from cache
     */
    suspend fun clearTeams()
}
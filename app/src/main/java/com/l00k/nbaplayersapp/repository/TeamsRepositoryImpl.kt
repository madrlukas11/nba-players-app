package com.l00k.nbaplayersapp.repository

import android.util.Log
import androidx.room.withTransaction
import com.l00k.nbaplayersapp.api.Team
import com.l00k.nbaplayersapp.db.NbaPlayersDatabase
import com.l00k.nbaplayersapp.db.TeamEntity
import com.l00k.nbaplayersapp.toTeamEntity

class TeamsRepositoryImpl(
    private val database: NbaPlayersDatabase,
) : TeamsRepository {

    override suspend fun getTeamById(id: Long): TeamEntity? {
        return try {
            database.withTransaction {
                database.teamsDao.getTeamById(id)
            }
        } catch (ex: Exception) {
            Log.e("TeamsRepositoryImpl","Could not get team by id: ${ex.message}")
            null
        }
    }

    override suspend fun saveTeams(
        teams: List<Team>,
        clearFirst: Boolean,
    ) {
        try {
            database.withTransaction {
                database.teamsDao.upsertAll(teams.toTeamEntity())
            }
        } catch (ex: Exception) {
            Log.w("TeamsRepositoryImpl ","Could not save teams: ${ex.message}")
        }
    }

    override suspend fun clearTeams() {
        try {
            database.withTransaction {
                database.teamsDao.clearAll()
            }
        } catch (ex: Exception) {
            Log.w("TeamsRepositoryImpl","Could not clear teams: ${ex.message}")
        }
    }
}
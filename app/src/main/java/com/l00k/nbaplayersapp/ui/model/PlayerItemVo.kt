package com.l00k.nbaplayersapp.ui.model

/**
 * Data holder for player list item to be displayed to user.
 *
 * @property id unique player identifier
 * @property fullName player's first name
 * @property position player's position in the field (e.g. C, F, Q..)
 * @property teamName player's current team
 */
data class PlayerItemVo(
    val id: Long,
    val fullName: String,
    val position: String?,
    val teamName: String,
)
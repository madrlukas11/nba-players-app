package com.l00k.nbaplayersapp.ui.component

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.l00k.nbaplayersapp.R
import com.l00k.nbaplayersapp.ui.base.PreviewSurface

@Composable
fun BackButton(
    modifier: Modifier = Modifier,
    navController: NavController
) {
    IconButton(
        modifier = modifier,
        onClick = {
            navController.navigateUp()
        }
    ) {
        Icon(
            imageVector = Icons.AutoMirrored.Filled.ArrowBack,
            contentDescription = stringResource(id = R.string.back_button_arrow)
        )
    }
}

@PreviewLightDark
@Composable
private fun BackButtonPreview() {
    PreviewSurface {
        BackButton(navController = rememberNavController())
    }
}
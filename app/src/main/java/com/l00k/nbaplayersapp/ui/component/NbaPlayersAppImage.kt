package com.l00k.nbaplayersapp.ui.component

import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.bumptech.glide.integration.compose.placeholder
import com.l00k.nbaplayersapp.R
import com.l00k.nbaplayersapp.ui.base.PreviewSurface

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun NbaPlayersAppImage(
    modifier: Modifier = Modifier,
    imageUrl: String?,
    contentDescription: String,
) {
    GlideImage(
        modifier = modifier.clip(RoundedCornerShape(8.dp)),
        model = imageUrl,
        contentDescription = contentDescription,
        failure = placeholder(
            drawable = ContextCompat.getDrawable(
                LocalContext.current,
                R.mipmap.ic_image_placeholder
            )
        )
    )
}

@PreviewLightDark
@Composable
private fun RickAndMortyImagePreview() {
    PreviewSurface {
        NbaPlayersAppImage(
            modifier = Modifier.size(100.dp),
            imageUrl = null,
            contentDescription = "player image"
        )
    }
}

package com.l00k.nbaplayersapp.ui

import com.l00k.nbaplayersapp.ui.model.PlayerDetailVo
import com.l00k.nbaplayersapp.ui.model.PlayerItemVo
import com.l00k.nbaplayersapp.ui.model.TeamDetailVo

/**
 * Provides mock view objects to display in composable previews
 */
object MockData {

    val playerList = listOf(
        PlayerItemVo(
            id = 1,
            fullName = "Michael Jordan",
            position = "F-G",
            teamName = "Chicago Bulls"
        ),
        PlayerItemVo(
            id = 2,
            fullName = "Kobe Bryant",
            position = "G",
            teamName = "Los Angeles Lakers"
        ),
        PlayerItemVo(
            id = 3,
            fullName = "Shaquille O'Neal",
            position = "C",
            teamName = "Los Angeles Lakers"
        ),
        PlayerItemVo(
            id = 4,
            fullName = "Dirk Nowitzki",
            position = "F",
            teamName = "Dallas Mavericks"
        )
    )

    val playerDetail = PlayerDetailVo(
        id = 1L,
        name = "Stephen Curry",
        position = "G",
        height = "6-2",
        weight = "185",
        jerseyNumber = "30",
        college = "Davidson",
        country = "USA",
        draftYear = "2009",
        draftRound = "1",
        draftNumber = "7",
        team = "Golden State Warriors",
        teamId = 1
    )

    val teamDetail = TeamDetailVo(
        name = "Denver Nuggets",
        city = "Denver",
        conference = "West",
        division = "Northwest",
        abbreviation = "DEN"
    )
}
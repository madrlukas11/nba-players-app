package com.l00k.nbaplayersapp.ui.model

/**
 * Data holder for player detail to be displayed to user
 *
 * @property id unique player identifier
 * @property name player's full name
 * @property position player's position in the field (e.g. C, F, Q..)
 * @property height player's height in feet
 * @property weight player's weight in pounds
 * @property jerseyNumber player's uniform number
 * @property college player's college that he/she is attending or graduated from
 * @property country player's nationality
 * @property draftYear year player was drafted
 * @property draftRound round player was drafted in
 * @property draftNumber number player was picked in draft
 * @property team player's current team
 * @property teamId unique identifier of player's team
 */
data class PlayerDetailVo(
    val id: Long,
    val name: String,
    val position: String,
    val height: String,
    val weight: String,
    val jerseyNumber: String,
    val college: String,
    val country: String,
    val draftYear: String?,
    val draftRound: String?,
    val draftNumber: String?,
    val team: String,
    val teamId: Long,
)

package com.l00k.nbaplayersapp.ui.base

import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import com.l00k.nbaplayersapp.ui.theme.NbaPlayersAppTheme

@Composable
fun PreviewSurface(
    content: @Composable () -> Unit,
) {
    NbaPlayersAppTheme {
        Surface {
            content()
        }
    }
}

package com.l00k.nbaplayersapp.ui.base

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

interface BaseViewModel<T, E : UIEvent> {

    val viewState: StateFlow<UIState<T>>

    fun onEvent(event: E)
}

class PreviewViewModel<T, E : UIEvent>(state: UIState<T>) : BaseViewModel<T, E> {

    override val viewState: StateFlow<UIState<T>> = MutableStateFlow(state)

    override fun onEvent(event: E) {}
}
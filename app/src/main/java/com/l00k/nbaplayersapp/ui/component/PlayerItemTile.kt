package com.l00k.nbaplayersapp.ui.component

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.l00k.nbaplayersapp.R
import com.l00k.nbaplayersapp.ui.model.PlayerItemVo

@Composable
fun PlayerItemTile(
    model: PlayerItemVo,
    onClick: (Long) -> Unit,
    modifier: Modifier = Modifier,
) {
    Card(
        modifier = modifier
            .clickable {
                onClick(model.id)
            },
        shape = RoundedCornerShape(16.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 10.dp
        )
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colorScheme.primaryContainer)
                .padding(8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            NbaPlayersAppImage(
                modifier = Modifier.size(92.dp),
                imageUrl = null,
                contentDescription = stringResource(id = R.string.player_image),
            )
            Column(
                modifier = Modifier.padding(
                    start = 12.dp,
                    top = 2.dp,
                    end = 2.dp
                )
            ) {
                Text(
                    text = model.fullName,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.W700,
                    color = MaterialTheme.colorScheme.primary
                )
                model.position?.let {
                    Text(
                        text = model.position,
                        color = MaterialTheme.colorScheme.secondary
                    )
                }
                Text(
                    text = model.teamName,
                    color = MaterialTheme.colorScheme.secondary,
                )
            }
        }
    }
}

@PreviewLightDark
@Composable
private fun PlayerItemTilePreview() {
//    PreviewSurface {
//        PlayerItemTile(
//            model = characterTileVos[0],
//            onClick = {}
//        )
//    }
}
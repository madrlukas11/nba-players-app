package com.l00k.nbaplayersapp.ui.component

import androidx.compose.foundation.background
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.l00k.nbaplayersapp.ui.base.PreviewSurface

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Toolbar(
    title: String,
    modifier: Modifier = Modifier,
    navController: NavController? = null,
) {
    Surface(
        modifier = modifier,
        shadowElevation = 10.dp
    ) {
        TopAppBar(
            colors = TopAppBarDefaults.topAppBarColors(
                containerColor = MaterialTheme.colorScheme.secondaryContainer
            ),
            title = {
                Text(
                    text = title,
                    color = MaterialTheme.colorScheme.primary,
                    fontSize = 20.sp,
                    fontWeight = FontWeight.W700
                )
            },
            navigationIcon = {
                navController?.let {
                    BackButton(
                        modifier = Modifier.background(
                            MaterialTheme.colorScheme.secondaryContainer
                        ),
                        navController = navController
                    )
                }
            }
        )
    }
}

@PreviewLightDark
@Composable
private fun ToolbarPreview() {
    PreviewSurface {
        Toolbar(
            title = "Players",
            navController = rememberNavController()
        )
    }
}
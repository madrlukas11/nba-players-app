package com.l00k.nbaplayersapp.ui.navigation

sealed class Destination(val route: String) {

    data object PlayerList : Destination("player_list")
    data object PlayerDetail : Destination("player_detail")
    data object TeamDetail : Destination("team_detail")

    fun withArgs(vararg args: Any): String {
        return buildString {
            append(route)
            args.forEach { arg ->
                append("/$arg")
            }
        }
    }
}
package com.l00k.nbaplayersapp.ui.component

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.l00k.nbaplayersapp.ui.base.UIState

@Composable
fun ScreenStateWrapper(
    state: UIState<*>,
    content: @Composable () -> Unit,
) {
    content()
    Crossfade(
        targetState = state,
        label = "screenStateAnimation"
    ) { targetState ->
        if (targetState.isLoading) {
            Box(
                modifier = Modifier.fillMaxSize()
            ) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .size(40.dp)
                        .align(Alignment.Center)
                )
            }
        }
    }
}
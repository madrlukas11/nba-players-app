package com.l00k.nbaplayersapp.ui.base

class UIState<T> (
    val data: T,
    val isLoading: Boolean = false
)
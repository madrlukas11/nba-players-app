package com.l00k.nbaplayersapp.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.l00k.nbaplayersapp.playerdetail.PlayerDetailScreen
import com.l00k.nbaplayersapp.playerdetail.PlayerDetailViewModel
import com.l00k.nbaplayersapp.playerlist.PlayerListScreen
import com.l00k.nbaplayersapp.playerlist.PlayerListViewModel
import com.l00k.nbaplayersapp.teamdetail.TeamDetailScreen
import com.l00k.nbaplayersapp.teamdetail.TeamDetailViewModel
import org.koin.androidx.compose.getViewModel
import org.koin.core.parameter.parametersOf

@Composable
fun NavigationConfig() {
    val navController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = Destination.PlayerList.route
    ) {
        composable(
            route = Destination.PlayerList.route,
        ) {
            PlayerListScreen(
                navController = navController,
                viewModel = getViewModel<PlayerListViewModel>()
            )
        }

        composable(
            route = Destination.PlayerDetail.route + "/{$PLAYER_ID_PARAM}",
            arguments = listOf(
                navArgument(PLAYER_ID_PARAM) {
                    type = NavType.LongType
                }
            )
        ) { entry ->
            PlayerDetailScreen(
                navController = navController,
                viewModel = getViewModel<PlayerDetailViewModel>(
                    parameters = {
                        parametersOf(entry.arguments?.getLong(PLAYER_ID_PARAM))
                    }
                )
            )
        }

        composable(
            route = Destination.TeamDetail.route + "/{$TEAM_ID_PARAM}",
            arguments = listOf(
                navArgument(TEAM_ID_PARAM) {
                    type = NavType.LongType
                }
            )
        ) {entry ->
            TeamDetailScreen(
                navController = navController,
                viewModel = getViewModel<TeamDetailViewModel>(
                    parameters = {
                        parametersOf(entry.arguments?.getLong(TEAM_ID_PARAM))
                    }
                )
            )
        }
    }
}

private const val PLAYER_ID_PARAM = "playerId"
private const val TEAM_ID_PARAM = "teamId"

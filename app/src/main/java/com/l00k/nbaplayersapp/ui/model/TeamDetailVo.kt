package com.l00k.nbaplayersapp.ui.model

/**
 * Data holder for team detail to be displayed to user.
 *
 * @property name team's full name
 * @property city team's home town
 * @property conference conference where the team belongs (East/West)
 * @property division division where the team belongs (e.g. Central, North West,.. )
 * @property abbreviation shortened name used for the team (e.g. when showing current score
 * to viewers on televised games)
 */
data class TeamDetailVo(
    val name: String,
    val city: String,
    val conference: String,
    val division: String,
    val abbreviation: String,
)
package com.l00k.nbaplayersapp.ui.theme

import androidx.compose.ui.graphics.Color

val Black = Color(0xFF000000)
val White = Color(0xFFFFFFFF)
val Grey = Color(0xFF2E2E2F)
val Grey2 = Color(0xFF666666)
val Grey3 = Color(0xFFF4F4F9)
val DarkGrey = Color(0xFF181819)
val LightGrey = Color(0xFF8B8B8C)

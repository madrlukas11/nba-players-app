package com.l00k.nbaplayersapp.playerlist

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.paging.compose.collectAsLazyPagingItems
import com.l00k.nbaplayersapp.R
import com.l00k.nbaplayersapp.ui.MockData
import com.l00k.nbaplayersapp.ui.base.BaseViewModel
import com.l00k.nbaplayersapp.ui.base.PreviewSurface
import com.l00k.nbaplayersapp.ui.base.PreviewViewModel
import com.l00k.nbaplayersapp.ui.base.UIState
import com.l00k.nbaplayersapp.ui.component.PlayerItemTile
import com.l00k.nbaplayersapp.ui.component.Toolbar
import kotlinx.coroutines.flow.flowOf

@Composable
fun PlayerListScreen(
    navController: NavController,
    viewModel: BaseViewModel<PlayerListScreenData, PlayerListScreenEvent>, // PlayerListViewModel
) {
    val viewState = viewModel.viewState.collectAsStateWithLifecycle()
    val characters = viewState.value.data.playersFlow.collectAsLazyPagingItems()

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Toolbar(
            title = stringResource(id = R.string.players),
        )
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            when (characters.loadState.refresh) {
                LoadState.Loading ->
                    CircularProgressIndicator(
                        modifier = Modifier
                            .align(Alignment.Center)
                            .size(40.dp)
                    )

                is LoadState.Error ->
                    Text(
                        modifier = Modifier.align(Alignment.Center),
                        text = stringResource(id = R.string.loading_error)
                    )

                is LoadState.NotLoading ->
                    LazyColumn(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(horizontal = 8.dp),
                        verticalArrangement = Arrangement.spacedBy(12.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        items(characters.itemCount) { index ->
                            characters[index]?.let { player ->
                                PlayerItemTile(
                                    modifier = Modifier
                                        .height(100.dp)
                                        .padding(
                                            top = if (index == 0) {
                                                8.dp
                                            } else {
                                                0.dp
                                            },
                                        ),
                                    model = player,
                                    onClick = {
                                        viewModel.onEvent(
                                            PlayerListScreenEvent.PlayerItemClicked(
                                                playerId = it,
                                                navController = navController
                                            )
                                        )
                                    }
                                )
                            }
                        }
                        item {
                            if (characters.loadState.append is LoadState.Loading) {
                                CircularProgressIndicator(
                                    modifier = Modifier.size(40.dp)
                                )
                            }
                        }
                    }
            }

        }
    }
}


// TODO("find a way to make this preview work")
@PreviewLightDark
@Composable
private fun PlayerListScreenPreview() {
    PreviewSurface {
        PlayerListScreen(
            navController = rememberNavController(),
            viewModel = PreviewViewModel(
                UIState(
                    data = PlayerListScreenData(
                        playersFlow = flowOf(
                            PagingData.from(
                                data = MockData.playerList
                            )
                        )
                    )
                )
            )
        )
    }
}

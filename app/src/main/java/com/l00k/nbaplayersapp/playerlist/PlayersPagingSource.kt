package com.l00k.nbaplayersapp.playerlist

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.l00k.nbaplayersapp.api.Player
import com.l00k.nbaplayersapp.repository.PlayersRepository
import com.l00k.nbaplayersapp.repository.TeamsRepository

/**
 * Loads paginated player list from api and saves it to cache.
 *
 * @property playersRepository repository that executes operations over players
 * @property teamsRepository repository that executes operations over teams
 *
 */
class PlayersPagingSource(
    private val playersRepository: PlayersRepository,
    private val teamsRepository: TeamsRepository,
) : PagingSource<Int, Player>() {

    /**
     * Last item in the list which the next loaded page will be
     * counted from (e.g. nextCursor is 70 and page size is 35 so items
     * 71-105 will be loaded in the next page).
     */
    private var nextCursor: Int = 0

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Player> {
        return try {
            val nextPageNumber = params.key ?: 1
            return playersRepository.fetchPlayers(nextCursor)?.let { data ->
                saveToCaches(data.players, params is LoadParams.Refresh)
                nextCursor = data.pagingData.nextCursor
                LoadResult.Page(
                    data = data.players,
                    prevKey = null,
                    nextKey = nextPageNumber + 1
                )
            } ?: LoadResult.Error(
                Exception("Error loading players")
            )
        } catch (ex: Exception) {
            LoadResult.Error(ex)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Player>): Int? {
        return state.anchorPosition?.let { position ->
            val page = state.closestPageToPosition(position)
            page?.prevKey?.minus(1) ?: page?.nextKey?.plus(1)
        }
    }

    /**
     * Saves players and teams to cache.
     *
     * @param players list of players to save together with their teams
     * @param willClearCaches whether caches should be cleared before saving
     */
    private suspend fun saveToCaches(
        players: List<Player>,
        willClearCaches: Boolean
    ) {
        playersRepository.savePlayers(
            players = players,
            clearFirst = willClearCaches
        )
        teamsRepository.saveTeams(
            teams = players.map { it.team },
            clearFirst = willClearCaches
        )
    }
}

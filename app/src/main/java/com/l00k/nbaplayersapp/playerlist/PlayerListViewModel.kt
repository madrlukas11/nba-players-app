package com.l00k.nbaplayersapp.playerlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.cachedIn
import androidx.paging.map
import com.l00k.nbaplayersapp.api.Player
import com.l00k.nbaplayersapp.toItemVo
import com.l00k.nbaplayersapp.ui.base.BaseViewModel
import com.l00k.nbaplayersapp.ui.base.UIState
import com.l00k.nbaplayersapp.ui.navigation.Destination
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map

class PlayerListViewModel(
    pager: Pager<Int, Player>,
) : BaseViewModel<PlayerListScreenData, PlayerListScreenEvent>, ViewModel() {

    private val _viewState = MutableStateFlow(
        UIState(
            data = PlayerListScreenData(
                playersFlow = pager.flow.map {
                    it.map { player ->
                        player.toItemVo()
                    }
                }.cachedIn(viewModelScope)
            )
        )
    )
    override val viewState = _viewState

    override fun onEvent(event: PlayerListScreenEvent) {
        when (event) {
            is PlayerListScreenEvent.PlayerItemClicked ->
                event.navController.navigate(
                    Destination.PlayerDetail.withArgs(event.playerId)
                )
        }
    }
}

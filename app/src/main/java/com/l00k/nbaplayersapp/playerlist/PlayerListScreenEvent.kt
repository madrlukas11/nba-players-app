package com.l00k.nbaplayersapp.playerlist

import androidx.navigation.NavController
import com.l00k.nbaplayersapp.ui.base.UIEvent

/**
 * UI events that happen on PlayerListScreen
 */
sealed interface PlayerListScreenEvent : UIEvent {

    data class PlayerItemClicked(
        val playerId: Long,
        val navController: NavController,
    ) : PlayerListScreenEvent
}
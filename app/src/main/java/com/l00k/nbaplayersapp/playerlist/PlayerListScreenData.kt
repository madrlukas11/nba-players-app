package com.l00k.nbaplayersapp.playerlist

import androidx.paging.PagingData
import com.l00k.nbaplayersapp.ui.model.PlayerItemVo
import kotlinx.coroutines.flow.Flow

/**
 * Data holder class to be displayed on PlayerListScreen.
 *
 * @property playersFlow paginated players data flow
 */
data class PlayerListScreenData(
    val playersFlow: Flow<PagingData<PlayerItemVo>>,
)
package com.l00k.nbaplayersapp

import com.l00k.nbaplayersapp.api.Player
import com.l00k.nbaplayersapp.api.Team
import com.l00k.nbaplayersapp.db.PlayerEntity
import com.l00k.nbaplayersapp.db.TeamEntity
import com.l00k.nbaplayersapp.ui.model.PlayerDetailVo
import com.l00k.nbaplayersapp.ui.model.PlayerItemVo
import com.l00k.nbaplayersapp.ui.model.TeamDetailVo

/**
 * Maps [Player] to [PlayerItemVo]
 */
fun Player.toItemVo() = PlayerItemVo(
    id = id,
    fullName = "$lastName $firstName",
    position = position.takeIf { it.isNotBlank() },
    teamName = team.name
)

/**
 * Maps [PlayerEntity] to [PlayerDetailVo]
 */
fun PlayerEntity.toPlayerDetailVo() = PlayerDetailVo(
    id = id,
    name = "$firstName $lastName",
    position = position.takeIf { it.isNotBlank() } ?: "N/A",
    height = height,
    weight = weight,
    jerseyNumber = jerseyNumber,
    college = college,
    country = country,
    draftYear = draftYear?.toString(),
    draftRound = draftRound?.toString(),
    draftNumber = draftNumber?.toString(),
    team = team,
    teamId = teamId
)

/**
 * Maps [List] of [Player] to [List] of [PlayerEntity]
 */
fun List<Player>.toPlayerEntity() = map {
    PlayerEntity(
        id = it.id,
        firstName = it.firstName,
        lastName = it.lastName,
        position = it.position,
        height = it.height,
        weight = it.weight,
        jerseyNumber = it.jerseyNumber,
        college = it.college,
        country = it.country,
        draftYear = it.draftYear,
        draftRound = it.draftRound,
        draftNumber = it.draftNumber,
        team = it.team.name,
        teamId = it.team.id,
    )
}

/**
 * Maps [List] of [Team] to [List] of [TeamEntity]
 */
fun List<Team>.toTeamEntity() = map {
    TeamEntity(
        id = it.id,
        name = it.name,
        city = it.city,
        conference = it.conference,
        division = it.division,
        abbreviation = it.abbreviation
    )
}

/**
 * Maps [TeamEntity] to [TeamDetailVo]
 */
fun TeamEntity.toTeamDetailVo() = TeamDetailVo(
    name = name,
    city = city,
    conference = conference,
    division = division,
    abbreviation = abbreviation
)


package com.l00k.nbaplayersapp.playerdetail

import com.l00k.nbaplayersapp.ui.model.PlayerDetailVo

/**
 * Data holder class to be displayed on PlayerDetailScreen.
 *
 * @property player player detail data to be displayed to user
 */
 data class PlayerDetailScreenData(
    val player: PlayerDetailVo?,
)

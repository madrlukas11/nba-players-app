package com.l00k.nbaplayersapp.playerdetail

import androidx.navigation.NavController
import com.l00k.nbaplayersapp.ui.base.UIEvent

/**
 * UI events that happen on PlayerDetailScreen
 */
sealed interface PlayerDetailScreenEvent : UIEvent {

    data class TeamNameClicked(
        val teamId: Long,
        val navController: NavController,
    ) : PlayerDetailScreenEvent
}
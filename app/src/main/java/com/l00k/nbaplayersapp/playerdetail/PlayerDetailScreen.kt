package com.l00k.nbaplayersapp.playerdetail

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.KeyboardArrowRight
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.l00k.nbaplayersapp.R
import com.l00k.nbaplayersapp.ui.MockData
import com.l00k.nbaplayersapp.ui.base.BaseViewModel
import com.l00k.nbaplayersapp.ui.base.PreviewSurface
import com.l00k.nbaplayersapp.ui.base.PreviewViewModel
import com.l00k.nbaplayersapp.ui.base.UIState
import com.l00k.nbaplayersapp.ui.component.InfoItem
import com.l00k.nbaplayersapp.ui.component.NbaPlayersAppImage
import com.l00k.nbaplayersapp.ui.component.ScreenStateWrapper
import com.l00k.nbaplayersapp.ui.component.Toolbar

@Composable
fun PlayerDetailScreen(
    navController: NavController,
    viewModel: BaseViewModel<PlayerDetailScreenData, PlayerDetailScreenEvent> // PlayerDetailViewModel
) {
    val viewState = viewModel.viewState.collectAsStateWithLifecycle()

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Toolbar(
            title = stringResource(id = R.string.player_detail),
            navController = navController
        )
        viewState.value.data.player?.let { player ->
            ScreenStateWrapper(state = viewState.value) {
                Box(
                    modifier = Modifier
                        .clip(RoundedCornerShape(12.dp))
                        .background(color = MaterialTheme.colorScheme.primaryContainer)
                        .padding(20.dp)
                ) {
                    Column(
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            NbaPlayersAppImage(
                                modifier = Modifier.size(100.dp),
                                imageUrl = null,
                                contentDescription = stringResource(id = R.string.player_image)
                            )
                            Column(
                                modifier = Modifier.padding(start = 12.dp),
                            ) {
                                Text(
                                    text = player.name,
                                    color = MaterialTheme.colorScheme.primary,
                                    fontSize = 22.sp,
                                    fontWeight = FontWeight.W500
                                )
                                Row(
                                    modifier = Modifier.clickable {
                                        viewModel.onEvent(
                                            PlayerDetailScreenEvent.TeamNameClicked(
                                                teamId = player.teamId,
                                                navController = navController
                                            )
                                        )
                                    },
                                    verticalAlignment = Alignment.Bottom
                                ) {
                                    Text(
                                        modifier = Modifier.padding(top = 8.dp),
                                        text = player.team,
                                        color = MaterialTheme.colorScheme.primary,
                                        fontSize = 16.sp,
                                        fontWeight = FontWeight.W500
                                    )
                                    Icon(
                                        modifier = Modifier.padding(start = 8.dp),
                                        imageVector = Icons.AutoMirrored.Filled.KeyboardArrowRight,
                                        contentDescription = stringResource(
                                            id = R.string.right_arrow_icon
                                        )
                                    )
                                }
                            }
                        }
                        HorizontalDivider(
                            modifier = Modifier.padding(vertical = 16.dp),
                            color = MaterialTheme.colorScheme.secondary
                        )
                        InfoItem(
                            modifier = Modifier.padding(top = 12.dp),
                            title = stringResource(id = R.string.position),
                            value = player.position
                        )
                        InfoItem(
                            modifier = Modifier.padding(top = 12.dp),
                            title = stringResource(id = R.string.height),
                            value = player.height
                        )
                        InfoItem(
                            modifier = Modifier.padding(top = 12.dp),
                            title = stringResource(id = R.string.weight),
                            value = player.weight
                        )
                        InfoItem(
                            modifier = Modifier.padding(top = 12.dp),
                            title = stringResource(id = R.string.jersey_number),
                            value = player.jerseyNumber
                        )
                        InfoItem(
                            modifier = Modifier.padding(top = 12.dp),
                            title = stringResource(id = R.string.college),
                            value = player.college
                        )
                        InfoItem(
                            modifier = Modifier.padding(top = 12.dp),
                            title = stringResource(id = R.string.country),
                            value = player.country
                        )
                        player.draftYear?.let {
                            InfoItem(
                                modifier = Modifier.padding(top = 12.dp),
                                title = stringResource(id = R.string.draft_year),
                                value = player.draftYear
                            )
                        }
                        player.draftRound?.let {
                            InfoItem(
                                modifier = Modifier.padding(top = 12.dp),
                                title = stringResource(id = R.string.draft_round),
                                value = player.draftRound
                            )
                        }
                        player.draftNumber?.let {
                            InfoItem(
                                modifier = Modifier.padding(top = 12.dp),
                                title = stringResource(id = R.string.draft_number),
                                value = player.draftNumber
                            )
                        }
                    }
                }
            }
        }
    }
}

@PreviewLightDark
@Composable
private fun PlayerDetailScreenPreview() {
    PreviewSurface {
        PlayerDetailScreen(
            navController = rememberNavController(),
            viewModel = PreviewViewModel(
                UIState(
                    data = PlayerDetailScreenData(
                        player = MockData.playerDetail
                    )
                )
            )
        )
    }
}

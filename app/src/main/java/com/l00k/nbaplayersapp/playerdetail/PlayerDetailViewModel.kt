package com.l00k.nbaplayersapp.playerdetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.l00k.nbaplayersapp.repository.PlayersRepository
import com.l00k.nbaplayersapp.toPlayerDetailVo
import com.l00k.nbaplayersapp.ui.base.BaseViewModel
import com.l00k.nbaplayersapp.ui.base.UIState
import com.l00k.nbaplayersapp.ui.navigation.Destination
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class PlayerDetailViewModel(
    playerId: Long,
    private val playersRepository: PlayersRepository,
) : BaseViewModel<PlayerDetailScreenData, PlayerDetailScreenEvent>, ViewModel() {

    init {
        viewModelScope.launch {
            playersRepository.getPlayerById(playerId)?.let { player ->
                _viewState.update {
                    UIState(
                        data = PlayerDetailScreenData(
                            player = player.toPlayerDetailVo()
                        ),
                        isLoading = false
                    )
                }
            }
        }
    }

    private val _viewState = MutableStateFlow(
        UIState(
            data = PlayerDetailScreenData(player = null),
            isLoading = true
        )
    )
    override val viewState = _viewState

    override fun onEvent(event: PlayerDetailScreenEvent) {
        when (event) {
            is PlayerDetailScreenEvent.TeamNameClicked ->
                event.navController.navigate(
                    Destination.TeamDetail.withArgs(event.teamId)
                )
        }
    }
}